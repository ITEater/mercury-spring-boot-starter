package top.codef.mongodb;

import java.util.LinkedList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;

public class ConverterContributor {

	private final List<Converter<?, ?>> contributedConverters = new LinkedList<Converter<?, ?>>();

	public void add(Converter<?, ?> converter) {
		contributedConverters.add(converter);
	}

	public void remove(Converter<?, ?> converter) {
		contributedConverters.remove(converter);
	}

	public List<Converter<?, ?>> getContributedConverters() {
		return contributedConverters;
	}
}
