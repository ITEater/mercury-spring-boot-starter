package top.codef.mongodb.converters;

import javax.money.MonetaryAmount;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public class MonetaryAmountReadingConverter extends AbstractMoneyReadingConverter<MonetaryAmount> {

	public MonetaryAmountReadingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	public MonetaryAmount convert(String source) {
		return mercuryMonetaryAmountFormat.parse(source);
	}

	@Override
	protected Class<MonetaryAmount> getReadingClass() {
		return null;
	}

}
