package top.codef.mongodb.converters;

import org.javamoney.moneta.FastMoney;
import org.springframework.data.convert.ReadingConverter;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

@ReadingConverter
public class FastMoneyReadingConverter extends AbstractMoneyReadingConverter<FastMoney> {

	public FastMoneyReadingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	protected Class<FastMoney> getReadingClass() {
		return FastMoney.class;
	}
}
