package top.codef.mongodb.converters;

import javax.money.MonetaryAmount;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

@WritingConverter
public class MoneyWritingConverter implements Converter<MonetaryAmount, String> {

	private final MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat;

	public MoneyWritingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		this.mercuryMonetaryAmountFormat = mercuryMonetaryAmountFormat;
	}

	@Override
	public String convert(MonetaryAmount source) {
		return mercuryMonetaryAmountFormat.format(source);
	}
}
