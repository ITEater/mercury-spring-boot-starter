package top.codef.mongodb.converters;

import org.javamoney.moneta.RoundedMoney;
import org.springframework.data.convert.ReadingConverter;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

@ReadingConverter
public class RoundedMoneyReadingConverter extends AbstractMoneyReadingConverter<RoundedMoney> {

	public RoundedMoneyReadingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	protected Class<RoundedMoney> getReadingClass() {
		return RoundedMoney.class;
	}

}
