package top.codef.mongodb.converters;

import org.javamoney.moneta.Money;
import org.springframework.data.convert.ReadingConverter;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

@ReadingConverter
public class MonetaMoneyReadingConverter extends AbstractMoneyReadingConverter<Money> {

	public MonetaMoneyReadingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	protected Class<Money> getReadingClass() {
		return Money.class;
	}
}
