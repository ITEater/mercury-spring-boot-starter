package top.codef.mongodb.converters;

import javax.money.MonetaryAmount;

import org.springframework.core.convert.converter.Converter;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public abstract class AbstractMoneyReadingConverter<T extends MonetaryAmount> implements Converter<String, T> {

	protected final MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat;

	public AbstractMoneyReadingConverter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		this.mercuryMonetaryAmountFormat = mercuryMonetaryAmountFormat;
	}

	@Override
	public T convert(String source) {
		T amount = mercuryMonetaryAmountFormat.parse(source, getReadingClass());
		return amount;
	}

	protected abstract Class<T> getReadingClass();
}
