package top.codef.mongodb;

@FunctionalInterface
public interface MongoConverterRegister {

	public void regist(ConverterContributor contributor);
}
