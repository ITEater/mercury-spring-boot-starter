package top.codef.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import top.codef.properties.enums.DateTimeFormat;

@ConfigurationProperties(prefix = "mercury.gson")
public class MercuryGsonProperties {

	private String datetimePattern;

	private DateTimeFormat dateTimeFormat = DateTimeFormat.ISO_LOCAL_DATE_TIME;

	public String getDatetimePattern() {
		return datetimePattern;
	}

	public void setDatetimePattern(String datetimePattern) {
		this.datetimePattern = datetimePattern;
	}

	public DateTimeFormat getDateTimeFormat() {
		return dateTimeFormat;
	}

	public void setDateTimeFormat(DateTimeFormat dateTimeFormat) {
		this.dateTimeFormat = dateTimeFormat;
	}

}
