package top.codef.properties;

import java.util.Locale;

import org.springframework.boot.context.properties.ConfigurationProperties;

import top.codef.enums.LocaleSelection;

@ConfigurationProperties(prefix = "mercury.money")
public class MercuryProperties {

	/**
	 * 区域选择
	 */
	private LocaleSelection locale = LocaleSelection.SIMPLIFIED_CHINESE;

	/**
	 * 钱的格式化模式
	 */
	private String pattern;

	/**
	 * 是否是严格的单位解析
	 */
	private boolean strictCurrency = false;
	

	public void setLocale(LocaleSelection locale) {
		this.locale = locale;
	}

	public LocaleSelection getLocale() {
		return locale;
	}

	public Locale currentLocale() {
		return locale.getLocale();
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public boolean isStrictCurrency() {
		return strictCurrency;
	}

	public void setStrictCurrency(boolean strictCurrency) {
		this.strictCurrency = strictCurrency;
	}

	@Override
	public String toString() {
		return "MercuryProperties [locale=" + locale + ", pattern=" + pattern + ", strictCurrency=" + strictCurrency
				+ "]";
	}

}
