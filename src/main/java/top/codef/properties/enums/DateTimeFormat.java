package top.codef.properties.enums;

import java.time.format.DateTimeFormatter;

public enum DateTimeFormat {

	ISO_DATE_TIME(DateTimeFormatter.ISO_DATE_TIME), ISO_INSTANT(DateTimeFormatter.ISO_INSTANT),
	ISO_LOCAL_DATE_TIME(DateTimeFormatter.ISO_LOCAL_DATE_TIME),;

	private final DateTimeFormatter dateTimeFormatter;

	private DateTimeFormat(DateTimeFormatter dateTimeFormatter) {
		this.dateTimeFormatter = dateTimeFormatter;
	}

	public DateTimeFormatter getDateTimeFormatter() {
		return dateTimeFormatter;
	}

}
