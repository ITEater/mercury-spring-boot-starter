package top.codef.properties.enums;

public enum MoneyDataType {

	BIGINT, VARCHAR, DECIMAL;
}
