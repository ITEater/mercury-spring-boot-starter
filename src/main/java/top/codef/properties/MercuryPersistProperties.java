package top.codef.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import top.codef.properties.enums.MoneyDataType;

@ConfigurationProperties(prefix = "mercury.money.persist")
public class MercuryPersistProperties {

	private boolean openMonetaryBasicTypePersist = false;

	private boolean persistOnMultiply100 = true;

	private MoneyDataType moneyDataType;

	public boolean isPersistOnMultiply100() {
		return persistOnMultiply100;
	}

	public void setPersistOnMultiply100(boolean persistOnMultiply100) {
		this.persistOnMultiply100 = persistOnMultiply100;
	}

	public MoneyDataType getMoneyDataType() {
		return moneyDataType;
	}

	public void setMoneyDataType(MoneyDataType moneyDataType) {
		this.moneyDataType = moneyDataType;
	}

	public boolean isOpenMonetaryBasicTypePersist() {
		return openMonetaryBasicTypePersist;
	}

	public void setOpenMonetaryBasicTypePersist(boolean openMonetaryBasicTypePersist) {
		this.openMonetaryBasicTypePersist = openMonetaryBasicTypePersist;
	}

	@Override
	public String toString() {
		return "MercuryPersistProperties [openMonetaryBasicTypePersist=" + openMonetaryBasicTypePersist
				+ ", persistOnMultiply100=" + persistOnMultiply100 + ", moneyDataType=" + moneyDataType + "]";
	}

}
