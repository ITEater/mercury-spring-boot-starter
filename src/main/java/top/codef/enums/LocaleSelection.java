package top.codef.enums;

import java.util.Locale;

public enum LocaleSelection {

	DEFAULE(Locale.getDefault()), ENGLISH(Locale.ENGLISH), FRENCH(Locale.FRENCH), GERMAN(Locale.GERMAN),
	ITALIAN(Locale.ITALIAN), JAPANESE(Locale.JAPANESE), KOREAN(Locale.KOREAN), CHINESE(Locale.CHINESE),
	SIMPLIFIED_CHINESE(Locale.SIMPLIFIED_CHINESE), TRADITIONAL_CHINESE(Locale.TRADITIONAL_CHINESE),
	FRANCE(Locale.FRANCE), GERMANY(Locale.GERMANY), ITALY(Locale.ITALY), JAPAN(Locale.JAPAN), KOREA(Locale.KOREA),
	UK(Locale.UK), US(Locale.US), CANADA(Locale.CANADA), CANADA_FRENCH(Locale.CANADA_FRENCH);

	private final Locale locale;

	private LocaleSelection(Locale locale) {
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}

}
