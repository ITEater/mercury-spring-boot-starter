package top.codef.gsonsupport.adapters;

import org.javamoney.moneta.RoundedMoney;

import top.codef.gsonsupport.MoneyGsonAdapter;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public final class RoundedMoneyAdapter extends MoneyGsonAdapter<RoundedMoney> {

	public RoundedMoneyAdapter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	public Class<RoundedMoney> getRawClass() {
		return RoundedMoney.class;
	}
}