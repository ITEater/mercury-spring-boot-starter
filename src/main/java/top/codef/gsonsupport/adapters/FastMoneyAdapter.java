package top.codef.gsonsupport.adapters;

import org.javamoney.moneta.FastMoney;

import top.codef.gsonsupport.MoneyGsonAdapter;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public final class FastMoneyAdapter extends MoneyGsonAdapter<FastMoney> {

	public FastMoneyAdapter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	public Class<FastMoney> getRawClass() {
		return FastMoney.class;
	}
}