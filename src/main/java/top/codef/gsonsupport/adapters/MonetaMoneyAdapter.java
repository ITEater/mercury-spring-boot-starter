package top.codef.gsonsupport.adapters;

import org.javamoney.moneta.Money;

import top.codef.gsonsupport.MoneyGsonAdapter;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public final class MonetaMoneyAdapter extends MoneyGsonAdapter<Money> {

	public MonetaMoneyAdapter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	public Class<Money> getRawClass() {
		return Money.class;
	}
}