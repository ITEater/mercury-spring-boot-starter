package top.codef.gsonsupport.adapters;

import javax.money.MonetaryAmount;

import top.codef.gsonsupport.MoneyGsonAdapter;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;

public class MonetaryAmountAdapter extends MoneyGsonAdapter<MonetaryAmount> {

	public MonetaryAmountAdapter(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryMonetaryAmountFormat);
	}

	@Override
	public Class<MonetaryAmount> getRawClass() {
		return MonetaryAmount.class;
	}
}