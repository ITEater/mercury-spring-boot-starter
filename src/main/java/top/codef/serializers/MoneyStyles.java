package top.codef.serializers;

import top.codef.anno.Money;
import top.codef.enums.MoneyFormatStyle;

public class MoneyStyles {

	public static MoneyFormatStyle in(Money money) {
		MoneyFormatStyle formatStyle = money.value();
		MoneyFormatStyle inFormatStyle = money.in();
		return formatStyle == MoneyFormatStyle.DEFAULT ? inFormatStyle : formatStyle;
	}

	public static MoneyFormatStyle out(Money money) {
		MoneyFormatStyle formatStyle = money.value();
		MoneyFormatStyle inFormatStyle = money.out();
		return formatStyle == MoneyFormatStyle.DEFAULT ? inFormatStyle : formatStyle;
	}
}
