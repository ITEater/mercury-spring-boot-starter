package top.codef.serializers;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.FastMoney;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import top.codef.enums.MoneyFormatStyle;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class DefaultMoneyDeserializer extends AbstractMoneyDeserializer<MonetaryAmount> {

	private static final long serialVersionUID = -3540975432131023637L;

	public DefaultMoneyDeserializer(Class<? extends MonetaryAmount> vc, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(vc, mercuryProperties, monetaryAmountFormat);
	}

	public DefaultMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MoneyFormatStyle style, MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, style, monetaryAmountFormat);
	}

	public DefaultMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, monetaryAmountFormat);
	}

	@Override
	protected FastMoney genericMoney(MonetaryAmount monetaryAmount) {
		return FastMoney.from(monetaryAmount);
	}

}
