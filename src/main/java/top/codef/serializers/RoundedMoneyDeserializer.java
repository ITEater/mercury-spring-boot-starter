package top.codef.serializers;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.RoundedMoney;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import top.codef.enums.MoneyFormatStyle;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class RoundedMoneyDeserializer extends AbstractMoneyDeserializer<RoundedMoney> {

	private static final long serialVersionUID = 6991945793943601500L;

	public RoundedMoneyDeserializer(Class<? extends MonetaryAmount> vc, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(vc, mercuryProperties, monetaryAmountFormat);
	}

	public RoundedMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MoneyFormatStyle style, MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, style, monetaryAmountFormat);
	}

	public RoundedMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, monetaryAmountFormat);
	}

	@Override
	protected RoundedMoney genericMoney(MonetaryAmount monetaryAmount) {
		return RoundedMoney.from(monetaryAmount);
	}
}
