package top.codef.serializers;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import top.codef.enums.MoneyFormatStyle;
import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class MonetaMoneyDeserializer extends AbstractMoneyDeserializer<Money> {

	private static final long serialVersionUID = -8371907669953654122L;

	public MonetaMoneyDeserializer(Class<? extends MonetaryAmount> vc, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(vc, mercuryProperties, monetaryAmountFormat);
	}

	public MonetaMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MoneyFormatStyle style, MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, style, monetaryAmountFormat);
	}

	public MonetaMoneyDeserializer(StdDeserializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MercuryMonetaryAmountFormat monetaryAmountFormat) {
		super(src, mercuryProperties, monetaryAmountFormat);
	}

	@Override
	protected Money genericMoney(MonetaryAmount monetaryAmount) {
		return Money.from(monetaryAmount);
	}

}
