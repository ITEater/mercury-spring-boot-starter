package top.codef.serializers;

import javax.money.MonetaryAmount;
import javax.money.format.MonetaryAmountFormat;

import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import top.codef.enums.MoneyFormatStyle;
import top.codef.properties.MercuryProperties;

public class DefaultMoneySerializer extends AbstractMoneySerializer<MonetaryAmount> {

	protected DefaultMoneySerializer(Class<? extends MonetaryAmount> t, boolean dummy,
			MercuryProperties mercuryProperties, MonetaryAmountFormat amountFormat) {
		super(t, dummy, mercuryProperties, amountFormat);
	}

	private static final long serialVersionUID = 5606811944429671727L;

	@Override
	protected AbstractMoneySerializer<?> withStyle(MoneyFormatStyle moneyFormatStyle) {
		return new DefaultMoneySerializer(this, mercuryProperties, moneyFormatStyle, amountFormat);
	}

	public DefaultMoneySerializer(Class<MonetaryAmount> t, MercuryProperties mercuryProperties,
			MonetaryAmountFormat amountFormat) {
		super(t, mercuryProperties, amountFormat);
	}

	public DefaultMoneySerializer(StdSerializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MonetaryAmountFormat amountFormat) {
		super(src, mercuryProperties, amountFormat);
	}

	public DefaultMoneySerializer(StdSerializer<? extends MonetaryAmount> src, MercuryProperties mercuryProperties,
			MoneyFormatStyle style, MonetaryAmountFormat amountFormat) {
		super(src, mercuryProperties, style, amountFormat);
	}

}
