package top.codef.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.mongodb.ConverterContributor;
import top.codef.mongodb.MongoConverterRegister;
import top.codef.mongodb.converters.FastMoneyReadingConverter;
import top.codef.mongodb.converters.MonetaMoneyReadingConverter;
import top.codef.mongodb.converters.MonetaryAmountReadingConverter;
import top.codef.mongodb.converters.MoneyWritingConverter;
import top.codef.mongodb.converters.RoundedMoneyReadingConverter;

@Configuration
@ConditionalOnClass({ MongoTemplate.class, MongoCustomConversions.class })
public class MongodbConverterConfig implements MongoConverterRegister {

	@Autowired
	private MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat;

	@Override
	public void regist(ConverterContributor contributor) {
		FastMoneyReadingConverter fastMoneyReadingConverter = new FastMoneyReadingConverter(
				mercuryMonetaryAmountFormat);
		MonetaMoneyReadingConverter monetaMoneyReadingConverter = new MonetaMoneyReadingConverter(
				mercuryMonetaryAmountFormat);
		MonetaryAmountReadingConverter monetaryAmountReadingConverter = new MonetaryAmountReadingConverter(
				mercuryMonetaryAmountFormat);
		RoundedMoneyReadingConverter roundedMoneyReadingConverter = new RoundedMoneyReadingConverter(
				mercuryMonetaryAmountFormat);
		MoneyWritingConverter moneyWritingConverter = new MoneyWritingConverter(mercuryMonetaryAmountFormat);
		contributor.add(fastMoneyReadingConverter);
		contributor.add(monetaMoneyReadingConverter);
		contributor.add(monetaryAmountReadingConverter);
		contributor.add(roundedMoneyReadingConverter);
		contributor.add(moneyWritingConverter);
	}
}
