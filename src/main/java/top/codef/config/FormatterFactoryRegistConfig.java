package top.codef.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import top.codef.formatterfactories.MoneyFormatterFactory;

@Configuration
public class FormatterFactoryRegistConfig implements WebMvcConfigurer {

	@Autowired
	private MoneyFormatterFactory moneyFormatterFactory;

	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatterForFieldAnnotation(moneyFormatterFactory);
	}

}
