package top.codef.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.hibernate.type.BasicType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.persist.contributors.MonetaryTypeContributor;
import top.codef.properties.MercuryPersistProperties;

@Configuration
@EnableConfigurationProperties(MercuryPersistProperties.class)
@ConditionalOnClass({ SessionFactory.class, BasicType.class, HibernatePropertiesCustomizer.class })
@ConditionalOnProperty(name = "mercury.money.persist.open-monetary-basic-type-persist", havingValue = "true")
public class MonetaryPersistConfig {

	@Autowired
	private MercuryPersistProperties mercuryPersistProperties;

	private final Log logger = LogFactory.getLog(MonetaryPersistConfig.class);

	@Bean
	@ConditionalOnMissingBean
	public MonetaryTypeContributor monetaryTypeContributor(MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		logger.debug("创建类型贡献者");
		MonetaryTypeContributor monetaryTypeContributor = new MonetaryTypeContributor(mercuryPersistProperties,
				mercuryMonetaryAmountFormat);
		return monetaryTypeContributor;
	}

}
