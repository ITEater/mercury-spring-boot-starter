package top.codef.config;

import java.util.List;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import top.codef.mongodb.ConverterContributor;
import top.codef.mongodb.MongoConverterRegister;

@Configuration
@ConditionalOnClass({ MongoTemplate.class, MongoCustomConversions.class })
@AutoConfigureBefore(MongoAutoConfiguration.class)
public class MongodbCustomConvertionConfig {

	@Bean
	public MongoCustomConversions mongoCustomConversions(List<MongoConverterRegister> mongoConverterRegisters,
			ConverterContributor converterContributor) {
		mongoConverterRegisters.forEach(x -> x.regist(converterContributor));
		MongoCustomConversions mongoCustomConversions = new MongoCustomConversions(
				converterContributor.getContributedConverters());
		return mongoCustomConversions;
	}
}
