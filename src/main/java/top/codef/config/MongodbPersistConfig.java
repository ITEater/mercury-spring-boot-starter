package top.codef.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import top.codef.mongodb.ConverterContributor;

@Configuration
@ConditionalOnClass({ Converter.class })
public class MongodbPersistConfig {
	
	@Bean
	public ConverterContributor converterContributor() {
		return new ConverterContributor();
	}
}
