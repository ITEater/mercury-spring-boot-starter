package top.codef.persist.basictypes;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.JavaType;
import org.hibernate.type.descriptor.jdbc.JdbcType;
import org.javamoney.moneta.RoundedMoney;

public class RoundedMoneyType extends AbstractSingleColumnStandardBasicType<RoundedMoney> {

	private static final long serialVersionUID = -654953005506725325L;

	public RoundedMoneyType(JdbcType sqlTypeDescriptor, JavaType<RoundedMoney> javaTypeDescriptor) {
		super(sqlTypeDescriptor, javaTypeDescriptor);
	}

	@Override
	public String getName() {
		return "RoundedMoney";
	}

}
