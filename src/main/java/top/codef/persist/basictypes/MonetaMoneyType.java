package top.codef.persist.basictypes;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.JavaType;
import org.hibernate.type.descriptor.jdbc.JdbcType;
import org.javamoney.moneta.Money;

public class MonetaMoneyType extends AbstractSingleColumnStandardBasicType<Money> {

	private static final long serialVersionUID = 7329230833568511553L;

	public MonetaMoneyType(JdbcType jdbcType, JavaType<Money> javaTypeDescriptor) {
		super(jdbcType, javaTypeDescriptor);
	}

	@Override
	public String getName() {
		return "Money";
	}

}
