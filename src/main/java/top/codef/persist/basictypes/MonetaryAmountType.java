package top.codef.persist.basictypes;

import javax.money.MonetaryAmount;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.JavaType;
import org.hibernate.type.descriptor.jdbc.JdbcType;

public class MonetaryAmountType extends AbstractSingleColumnStandardBasicType<MonetaryAmount> {

	private static final long serialVersionUID = -2184178708571236774L;

	public MonetaryAmountType(JdbcType sqlTypeDescriptor, JavaType<MonetaryAmount> javaTypeDescriptor) {
		super(sqlTypeDescriptor, javaTypeDescriptor);
	}

	@Override
	public String getName() {
		return "MonetaryAmount";
	}

}
