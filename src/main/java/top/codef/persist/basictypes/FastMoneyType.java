package top.codef.persist.basictypes;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.JavaType;
import org.hibernate.type.descriptor.jdbc.JdbcType;
import org.javamoney.moneta.FastMoney;

public class FastMoneyType extends AbstractSingleColumnStandardBasicType<FastMoney> {

	private static final long serialVersionUID = 4371827064868891645L;

	public FastMoneyType(JdbcType sqlTypeDescriptor, JavaType<FastMoney> javaTypeDescriptor) {
		super(sqlTypeDescriptor, javaTypeDescriptor);
	}

	@Override
	public String getName() {
		return "FastMoney";
	}

}
