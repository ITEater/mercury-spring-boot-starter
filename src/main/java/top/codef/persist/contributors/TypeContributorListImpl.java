//package top.codef.persist.contributors;
//
//import java.util.LinkedList;
//import java.util.List;
//
//import org.hibernate.boot.model.TypeContributor;
//import org.hibernate.jpa.boot.spi.TypeContributorList;
//
//@SuppressWarnings("removal")
//public class TypeContributorListImpl implements TypeContributorList {
//
//	private final List<TypeContributor> list = new LinkedList<>();
//
//	@Override
//	public List<TypeContributor> getTypeContributors() {
//		return list;
//	}
//
//	public void add(TypeContributor contributor) {
//		list.add(contributor);
//	}
//
//}
