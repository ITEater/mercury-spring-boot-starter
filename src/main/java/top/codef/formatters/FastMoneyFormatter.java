package top.codef.formatters;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.FastMoney;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class FastMoneyFormatter extends MoneyFormatter<FastMoney> {

	public FastMoneyFormatter(MercuryProperties mercuryProperties, Class<FastMoney> clazz , MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryProperties, clazz , mercuryMonetaryAmountFormat);
	}

	@Override
	public FastMoney from(MonetaryAmount amount) {
		return FastMoney.from(amount);
	}

}
