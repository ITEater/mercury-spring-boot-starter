package top.codef.formatters;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.RoundedMoney;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class RoundedMoneyFormmatter extends MoneyFormatter<RoundedMoney> {

	public RoundedMoneyFormmatter(MercuryProperties mercuryProperties, Class<RoundedMoney> clazz,
			MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryProperties, clazz, mercuryMonetaryAmountFormat);
	}

	@Override
	public RoundedMoney from(MonetaryAmount amount) {
		return RoundedMoney.from(amount);
	}
}
