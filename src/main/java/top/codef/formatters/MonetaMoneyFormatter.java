package top.codef.formatters;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

import top.codef.money.interfaces.MercuryMonetaryAmountFormat;
import top.codef.properties.MercuryProperties;

public class MonetaMoneyFormatter extends MoneyFormatter<Money> {

	public MonetaMoneyFormatter(MercuryProperties mercuryProperties, Class<Money> clazz,
			MercuryMonetaryAmountFormat mercuryMonetaryAmountFormat) {
		super(mercuryProperties, clazz, mercuryMonetaryAmountFormat);
	}

	@Override
	public Money from(MonetaryAmount amount) {
		return Money.from(amount);
	}
}
