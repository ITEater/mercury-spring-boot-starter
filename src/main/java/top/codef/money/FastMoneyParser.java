package top.codef.money;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatContext;

import org.javamoney.moneta.FastMoney;

public class FastMoneyParser extends MercuryMoneyParser {

	public FastMoneyParser(AmountFormatContext amountFormatContext) {
		super(amountFormatContext);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected FastMoney from(MonetaryAmount monetaryAmount) {
		return FastMoney.from(monetaryAmount);
	}
}
