package top.codef.money;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatContext;

import org.javamoney.moneta.RoundedMoney;

public class RoundedMoneyParser extends MercuryMoneyParser {

	public RoundedMoneyParser(AmountFormatContext amountFormatContext) {
		super(amountFormatContext);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected RoundedMoney from(MonetaryAmount monetaryAmount) {
		return RoundedMoney.from(monetaryAmount);
	}

}
