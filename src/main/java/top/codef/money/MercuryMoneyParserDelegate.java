package top.codef.money;

import java.util.HashMap;
import java.util.Map;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatContext;

import org.javamoney.moneta.FastMoney;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.RoundedMoney;

import top.codef.money.interfaces.MoneyPaser;

public class MercuryMoneyParserDelegate extends MercuryMoneyParser {

	Map<Class<? extends MonetaryAmount>, MoneyPaser> map = new HashMap<>();

	public MercuryMoneyParserDelegate(AmountFormatContext amountFormatContext) {
		super(amountFormatContext);
		map.put(FastMoney.class, new FastMoneyParser(amountFormatContext));
		map.put(Money.class, new MonetaMoneyParser(amountFormatContext));
		map.put(RoundedMoney.class, new RoundedMoneyParser(amountFormatContext));
		map.put(MonetaryAmount.class, map.get(FastMoney.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected MonetaryAmount from(MonetaryAmount monetaryAmount) {
		return FastMoney.from(monetaryAmount);
	}

	public <T extends MonetaryAmount> T parse(String text, Class<? extends MonetaryAmount> clazz) {
		T t = map.get(clazz).parse(text);
		return t;
	}
}
