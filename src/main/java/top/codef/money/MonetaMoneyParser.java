package top.codef.money;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatContext;

import org.javamoney.moneta.Money;

public class MonetaMoneyParser extends MercuryMoneyParser {

	public MonetaMoneyParser(AmountFormatContext amountFormatContext) {
		super(amountFormatContext);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Money from(MonetaryAmount monetaryAmount) {
		return Money.from(monetaryAmount);
	}

}
