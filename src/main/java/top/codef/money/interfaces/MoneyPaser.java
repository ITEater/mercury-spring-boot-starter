package top.codef.money.interfaces;

import javax.money.MonetaryAmount;

@FunctionalInterface
public interface MoneyPaser {

	<T extends MonetaryAmount> T parse(String text);

}
