package top.codef.money.interfaces;

import javax.money.MonetaryAmount;
import javax.money.format.MonetaryAmountFormat;

public interface MercuryMonetaryAmountFormat extends MonetaryAmountFormat {

	public <T extends MonetaryAmount> T parse(CharSequence text, Class<? extends MonetaryAmount> clazz);
}
