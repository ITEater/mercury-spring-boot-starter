package top.codef.money.interfaces;

import javax.money.MonetaryAmount;

@FunctionalInterface
public interface MoneyPrinter {

	public String print(MonetaryAmount amount);
}
