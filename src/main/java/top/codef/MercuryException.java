package top.codef;

public class MercuryException extends RuntimeException {

	public MercuryException() {
	}

	public MercuryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MercuryException(String message, Throwable cause) {
		super(message, cause);
	}

	public MercuryException(String message) {
		super(message);
	}

	public MercuryException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = -2356701822996020135L;

}
